import { atom } from "recoil";

export const valueSearchProduct = atom({
  key: "valueSearchProduct",
  default: "",
});
