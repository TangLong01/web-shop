import { t } from "@lingui/macro";
import { Select } from "antd";
import { ChangeLanguage, GetCurrentLanguage } from "../../utils/language";

export default function LangSwitcher({ className }) {
  const curLang = GetCurrentLanguage();
  const listLang = [
    { value: "en", label: t`English` },
    { value: "vi", label: t`Vietnamese` },
  ];

  return (
    <div className={`${className} changeLang`}>
      <Select
        options={listLang}
        value={curLang}
        onChange={(e) => {
          ChangeLanguage(e);
        }}
        className="lgScreen:w-[120px] mdScreen:w-[110px] smScreen:w-[100px] w-[90px]"
      />
    </div>
  );
}
