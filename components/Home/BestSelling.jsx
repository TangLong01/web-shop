import React, { useEffect, useState } from "react";
import TitleHome from "../TitleHome/TitleHome";
import { Trans, t } from "@lingui/macro";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import "@splidejs/splide/dist/css/themes/splide-default.min.css";
import { Rate } from "antd";
import { GetCurrentLanguage } from "../../utils/language";
import numeral from "numeral";

const BestSelling = ({ data }) => {
  const curLang = GetCurrentLanguage();

  const [dataBestSelling, setDataBestSelling] = useState([]);
  const [hoveredId, setHoveredId] = useState(-1);

  useEffect(() => {
    const filteredData = data.filter((item) => item.noOfPurchases > 50);
    const sortedData = filteredData
      .slice()
      .sort((a, b) => b.noOfPurchases - a.noOfPurchases);
    setDataBestSelling(sortedData);
  }, [data]);

  return (
    <div className="pb-20">
      {dataBestSelling.length > 0 && (
        <div className="w-full pb-20 pt-10">
          <TitleHome
            LTitle={t`This Month`}
            STitle={t`Best Selling Products`}
            noOfProducts={dataBestSelling.length}
          />
          <Splide
            options={{
              type: "slide",
              perPage: 4,
              direction: "ltr",
              gap: "3%",
              arrows: false,
              pagination: false,
            }}
          >
            {dataBestSelling.map((item, index) => (
              <SplideSlide key={index}>
                <div
                  onMouseEnter={() => setHoveredId(item.id)}
                  onMouseLeave={() => setHoveredId(-1)}
                  className="flex flex-col px-2 py-4 gap-y-2 rounded-md hover:text-white hover:cursor-pointer hover:bg-black05"
                >
                  <div className="w-full relative">
                    {item.sale !== 0 && (
                      <div className="absolute top-2 left-2 z-10 bg-red text-white lgScreen:px-3 lgScreen:py-1 px-2 py-0 rounded-sm">
                        -{item.sale}%
                      </div>
                    )}
                    <div className="absolute top-2 right-2 z-10 bg-blue-100 text-white p-1 rounded-full">
                      <img src="/img/home/heart.svg" />
                    </div>
                    <img
                      src={item.image}
                      className="w-full aspect-square rounded-md drop-shadow-lg"
                    />
                    {hoveredId === item.id && (
                      <div className="flex justify-center items-center textBigger absolute bottom-0 left-0 w-full h-[12%] z-10 bg-black text-white rounded-b-md">
                        <Trans id="Add To Cart" />
                      </div>
                    )}
                  </div>
                  <div className="font-bold textBigger mt-2">
                    {curLang === "vi" ? item.nameVi : item.nameEn}
                  </div>
                  <div className="flex gap-x-4">
                    <div className="text-red">
                      {numeral(
                        item.price - (item.price * item.sale) / 100
                      ).format("0,0")}
                      ₫
                    </div>
                    {item.sale !== 0 && (
                      <div className="line-through">
                        {numeral(item.price).format("0,0")}₫
                      </div>
                    )}
                  </div>
                  <div>
                    ({item.noOfPurchases} <Trans id="purchases" />)
                  </div>
                  <Rate defaultValue={item.rating} allowHalf disabled />
                </div>
              </SplideSlide>
            ))}
          </Splide>
        </div>
      )}
      <img
        src="/img/home/halloween2.png"
        className="hover:cursor-pointer rounded-md"
      />
    </div>
  );
};

export default BestSelling;
