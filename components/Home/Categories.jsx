import React, { useEffect, useRef, useState } from "react";
import TitleHome from "../TitleHome/TitleHome";
import { t } from "@lingui/macro";
import "@splidejs/splide/dist/css/themes/splide-default.min.css";
import { TYPE_LIST } from "../../constants/type";

const Categories = ({ chooseType, setChooseType, categoriesRef }) => {
  const [isScrolledThrough, setIsScrolledThrough] = useState(false);
  useEffect(() => {
    const handleScroll = () => {
      if (categoriesRef.current) {
        const categoriesTop = categoriesRef.current.getBoundingClientRect().top;
        const categoriesBottom =
          categoriesRef.current.getBoundingClientRect().bottom;
        const isVisible =
          categoriesTop < window.innerHeight && categoriesBottom > 200;

        if (!isVisible) {
          setIsScrolledThrough(true);
        } else {
          setIsScrolledThrough(false);
        }
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const BoxType = ({ nameType, type }) => {
    return (
      <div
        className={`border-[1px] border-black05 rounded-lg hover:cursor-pointer ${
          isScrolledThrough
            ? "aspect-2 gap-y-1 pt-1"
            : "aspect-square lgScreen:gap-y-3 gap-y-1"
        } 
        flex flex-col items-center justify-center hover:scale-105 ${
          chooseType === type ? "bg-red" : "bg-white"
        }`}
        onClick={() => setChooseType(type)}
      >
        <img
          src={`/img/home/${TYPE_LIST[type]}.png`}
          className={
            isScrolledThrough
              ? "lgScreen:w-[30%] w-[25%] pb-1"
              : "lgScreen:w-[50%] w-[40%]"
          }
        />
        {!isScrolledThrough && (
          <div className="textBigger font-bold">{nameType}</div>
        )}
      </div>
    );
  };

  return (
    <div
      className="w-full border-b-[1px] border-black05 pb-20"
      ref={categoriesRef}
    >
      <TitleHome LTitle={t`Categories`} STitle={t`Browse By Category`} />
      <div
        className={`flex justify-between ${
          isScrolledThrough
            ? "fixed top-[55px] left-0 gap-x-[3%] bg-menuBg drop-shadow-2xl z-50 py-1 px-[25%] border-b-[1px] border-black05"
            : "py-4 gap-x-[4%]"
        }`}
      >
        <BoxType nameType={t`All`} type={0} />
        <BoxType nameType={t`Clothe`} type={1} />
        <BoxType nameType={t`Shoe`} type={2} />
        <BoxType nameType={t`Socks`} type={3} />
        <BoxType nameType={t`Watch`} type={4} />
        <BoxType nameType={t`Glasses`} type={5} />
        <BoxType nameType={t`Hat`} type={6} />
      </div>
    </div>
  );
};

export default Categories;
