import React from "react";
import TitleHome from "../TitleHome/TitleHome";
import { t } from "@lingui/macro";

const NewArrival = () => {
  return (
    <div className="pt-10">
      <TitleHome LTitle={t`Featured`} STitle={t`New Arrival`} />
      <img
        src="/img/home/new-arrival.png"
        className="hover:cursor-pointer rounded-md"
      />
    </div>
  );
};

export default NewArrival;
