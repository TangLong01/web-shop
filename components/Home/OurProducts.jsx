import React, { useEffect, useState } from "react";
import TitleHome from "../TitleHome/TitleHome";
import { Trans, t } from "@lingui/macro";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import "@splidejs/splide/dist/css/themes/splide-default.min.css";
import { Rate } from "antd";
import { GetCurrentLanguage } from "../../utils/language";
import numeral from "numeral";
import { Pagination } from "antd";
import { getTotalProducts } from "../../utils/request";
import { valueSearchProduct } from "../../atoms/searchProduct";
import { useRecoilValue } from "recoil";

const OurProducts = ({
  dataOurProducts,
  currentPage,
  setCurrentPage,
  pageSize,
  setPageSize,
  chooseType,
}) => {
  const curLang = GetCurrentLanguage();
  const searchValue = useRecoilValue(valueSearchProduct);

  const [hoveredId, setHoveredId] = useState(-1);
  const [totalProducts, setTotalProducts] = useState(-1);
  const [dataSearch, setDataSearch] = useState([]);

  const itemRender = (_, type, originalElement) => {
    if (type === "prev") {
      return <a>Previous</a>;
    }
    if (type === "next") {
      return <a>Next</a>;
    }
    return originalElement;
  };

  useEffect(() => {
    getTotalProducts().then((res) => setTotalProducts(res.data.totalProducts));
    if (searchValue !== "") {
      if (curLang === "vi") {
        const returnDataSearch = dataOurProducts.filter((product) =>
          product.nameVi.toLowerCase().includes(searchValue)
        );
        setDataSearch(returnDataSearch);
      } else {
        const returnDataSearch = dataOurProducts.filter((product) =>
          product.nameEn.toLowerCase().includes(searchValue)
        );
        setDataSearch(returnDataSearch);
      }
    } else setDataSearch([]);
  }, [searchValue, chooseType]);

  return (
    <div className="w-full border-b-[1px] border-black05 pb-20 pt-10">
      {dataSearch.length === 0 ? (
        <div>
          <TitleHome
            LTitle={t`Our Products`}
            STitle={t`Explore Our Products`}
            noOfProducts={dataOurProducts.length}
          />
          <div
            className={`grid grid-cols-4 ${
              dataOurProducts.length > 4 ? "grid-rows-2" : "grid-rows-1"
            }`}
          >
            {dataOurProducts.map((item, index) => (
              <div
                key={index}
                onMouseEnter={() => setHoveredId(item.id)}
                onMouseLeave={() => setHoveredId(-1)}
                className="flex flex-col px-2 py-4 gap-y-2 rounded-md hover:text-white hover:cursor-pointer hover:bg-black05"
              >
                <div className="w-full relative">
                  {index < 5 &&
                    currentPage === 1 &&
                    item.id >= totalProducts - 5 && (
                      <div>
                        <div className="absolute top-2 left-2 z-10 bg-green-500 text-white lgScreen:px-3 lgScreen:py-1 px-2 py-0 rounded-sm">
                          NEW
                        </div>
                        {item.sale !== 0 && (
                          <div className="absolute top-2 lgScreen:left-20 left-16 z-10 bg-red text-white lgScreen:px-3 lgScreen:py-1 px-2 py-0 rounded-sm">
                            -{item.sale}%
                          </div>
                        )}
                      </div>
                    )}
                  {item.sale !== 0 && index >= 5 && (
                    <div className="absolute top-2 left-2 z-10 bg-red text-white lgScreen:px-3 lgScreen:py-1 px-2 py-0 rounded-sm">
                      -{item.sale}%
                    </div>
                  )}
                  <div className="absolute top-2 right-2 z-10 bg-blue-100 text-white p-1 rounded-full">
                    <img src="/img/home/heart.svg" />
                  </div>
                  <img
                    src={item.image}
                    className="w-full aspect-square rounded-md drop-shadow-lg"
                  />
                  {hoveredId === item.id && (
                    <div className="flex justify-center items-center textBigger absolute bottom-0 left-0 w-full h-[12%] z-10 bg-black text-white rounded-b-md">
                      <Trans id="Add To Cart" />
                    </div>
                  )}
                </div>
                <div className="font-bold textBigger mt-2">
                  {curLang === "vi" ? item.nameVi : item.nameEn}
                </div>
                <div className="flex gap-x-4">
                  <div className="text-red">
                    {numeral(
                      item.price - (item.price * item.sale) / 100
                    ).format("0,0")}
                    ₫
                  </div>
                  {item.sale !== 0 && (
                    <div className="line-through">
                      {numeral(item.price).format("0,0")}₫
                    </div>
                  )}
                </div>
                <div>
                  ({item.noOfPurchases} <Trans id="purchases" />)
                </div>
                <Rate defaultValue={item.rating} allowHalf disabled />
              </div>
            ))}
          </div>
          {dataOurProducts.length > 0 && (
            <div className="mt-4 w-full flex justify-end">
              <Pagination
                defaultCurrent={currentPage}
                total={
                  chooseType === 0 ? totalProducts : dataOurProducts.length
                }
                pageSize={pageSize}
                pageSizeOptions={[8, 16, 32, 64]}
                itemRender={itemRender}
                onChange={(page, pageSize) => {
                  setCurrentPage(page);
                  setPageSize(pageSize);
                }}
              />
            </div>
          )}
        </div>
      ) : (
        <div>
          <span className="font-bold textBigger">
            <Trans id="Your search: " />"{searchValue}"
          </span>
          <div
            className={`grid grid-cols-4 ${
              dataSearch.length > 4 ? "grid-rows-2" : "grid-rows-1"
            }`}
          >
            {dataSearch.map((item, index) => (
              <div
                key={index}
                onMouseEnter={() => setHoveredId(item.id)}
                onMouseLeave={() => setHoveredId(-1)}
                className="flex flex-col px-2 py-4 gap-y-2 rounded-md hover:text-white hover:cursor-pointer hover:bg-black05"
              >
                <div className="w-full relative">
                  {index < 5 &&
                    currentPage === 1 &&
                    item.id >= totalProducts - 5 && (
                      <div>
                        <div className="absolute top-2 left-2 z-10 bg-green-500 text-white lgScreen:px-3 lgScreen:py-1 px-2 py-0 rounded-sm">
                          NEW
                        </div>
                        {item.sale !== 0 && (
                          <div className="absolute top-2 lgScreen:left-20 left-16 z-10 bg-red text-white lgScreen:px-3 lgScreen:py-1 px-2 py-0 rounded-sm">
                            -{item.sale}%
                          </div>
                        )}
                      </div>
                    )}
                  {item.sale !== 0 && index >= 5 && (
                    <div className="absolute top-2 left-2 z-10 bg-red text-white lgScreen:px-3 lgScreen:py-1 px-2 py-0 rounded-sm">
                      -{item.sale}%
                    </div>
                  )}
                  <div className="absolute top-2 right-2 z-10 bg-blue-100 text-white p-1 rounded-full">
                    <img src="/img/home/heart.svg" />
                  </div>
                  <img
                    src={item.image}
                    className="w-full aspect-square rounded-md drop-shadow-lg"
                  />
                  {hoveredId === item.id && (
                    <div className="flex justify-center items-center textBigger absolute bottom-0 left-0 w-full h-[12%] z-10 bg-black text-white rounded-b-md">
                      <Trans id="Add To Cart" />
                    </div>
                  )}
                </div>
                <div className="font-bold textBigger mt-2">
                  {curLang === "vi" ? item.nameVi : item.nameEn}
                </div>
                <div className="flex gap-x-4">
                  <div className="text-red">
                    {numeral(
                      item.price - (item.price * item.sale) / 100
                    ).format("0,0")}
                    ₫
                  </div>
                  {item.sale !== 0 && (
                    <div className="line-through">
                      {numeral(item.price).format("0,0")}₫
                    </div>
                  )}
                </div>
                <div>
                  ({item.noOfPurchases} <Trans id="purchases" />)
                </div>
                <Rate defaultValue={item.rating} allowHalf disabled />
              </div>
            ))}
          </div>
          {dataSearch.length > 0 && (
            <div className="mt-4 w-full flex justify-end">
              <Pagination
                defaultCurrent={currentPage}
                total={dataSearch.length}
                pageSize={pageSize}
                pageSizeOptions={[8, 16, 32, 64]}
                itemRender={itemRender}
                onChange={(page, pageSize) => {
                  setCurrentPage(page);
                  setPageSize(pageSize);
                }}
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default OurProducts;
