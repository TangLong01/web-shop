import React from "react";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import { POSTER_LIST } from "../../constants/poster";

const Poster = () => {
  return (
    <div className="w-full mb-10">
      <Splide
        options={{
          type: "slide",
          perPage: 1,
          autoplay: true,
          interval: 4000,
          arrows: false,
          pagination: true,
          rewind: true,
        }}
      >
        {POSTER_LIST.map((item, index) => (
          <SplideSlide key={index}>
            <img src={item} className="hover:cursor-pointer w-full h-[650px]" />
          </SplideSlide>
        ))}
      </Splide>
    </div>
  );
};

export default Poster;
