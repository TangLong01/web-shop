import { Trans } from "@lingui/macro";
import React from "react";

const Treatment = () => {
  return (
    <div className="py-28 flex px-[10%] w-full justify-center">
      <div className="w-1/3 flex flex-col justify-center items-center gap-y-1">
        <img src="/img/home/delivery.svg" className="w-[25%] pb-2" />
        <div className="font-extrabold textBiggest">
          <Trans id="FREE AND FAST DELIVERY" />
        </div>
        <div className="">
          <Trans id="Free delivery for all orders over 200.000₫" />
        </div>
      </div>

      <div className="w-1/3 flex flex-col justify-center items-center gap-y-1">
        <img src="/img/home/support.svg" className="w-[25%] pb-2" />
        <div className="font-extrabold textBiggest">
          <Trans id="24/7 CUSTOMER SERVICE" />
        </div>
        <div className="">
          <Trans id="Friendly 24/7 customer support" />
        </div>
      </div>

      <div className="w-1/3 flex flex-col justify-center items-center gap-y-1">
        <img src="/img/home/money-back.svg" className="w-[25%] pb-2" />
        <div className="font-extrabold textBiggest">
          <Trans id="MONEY BACK GUARANTEE" />
        </div>
        <div className="">
          <Trans id="We reurn money within 30 days" />
        </div>
      </div>
    </div>
  );
};

export default Treatment;
