import { Trans } from "@lingui/macro";
import React from "react";

const InvalidInput = ({ invalid, invalitChild = [] }) => {
  return (
    <span className="text-sm text-red flex flex-col">
      <Trans id={invalid} />
      <span className="text-xs pl-2 flex flex-col">
        {invalitChild.length > 0 && (
          <>
            {invalitChild.map((item, index) => {
              return (
                <span key={index}>
                  • <Trans id={item} />
                </span>
              );
            })}
          </>
        )}
      </span>
    </span>
  );
};

export default InvalidInput;
