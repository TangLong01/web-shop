import { Trans } from "@lingui/macro";
import React from "react";

const Footer = () => {
  return (
    <div className="h-[310px] bg-black text-white flex flex-col">
      <div className="h-[88%] spacingContent border-b-[1px] border-gray flex justify-between mt-[20px]">
        <div className="flex flex-col items-start w-[15%] gap-y-2">
          <button className="font-bold text-3xl">
            <Trans id="Exclusive" />
          </button>
          <button className="font-bold">
            <Trans id="Subscribe" />
          </button>
          <div className="cursor-pointer">
            <Trans id="Get 10% off your first order" />
          </div>
        </div>

        <div className="flex flex-col items-start w-[27%] gap-y-2">
          <span className="font-bold">
            <Trans id="Support" />
          </span>
          <span className="w-[70%]">
            A17 Ta Quang Buu Street, Hai Ba Trung District, Hanoi
          </span>
          <span>hlongoo450@gmail.com</span>
          <span>0123-4567-8900</span>
        </div>

        <div className="flex flex-col items-start w-[20%] gap-y-2">
          <button className="font-bold">
            <Trans id="Account" />
          </button>
          <button>
            <Trans id="My Account" />
          </button>
          <div>
            <button>
              <Trans id="Login" />
            </button>{" "}
            /{" "}
            <button>
              <Trans id="Register" />
            </button>
          </div>
          <button>0123-4567-8900</button>
          <button>
            <Trans id="Cort" />
          </button>
          <button>
            <Trans id="Wishlist" />
          </button>
          <button>
            <Trans id="Shop" />
          </button>
        </div>

        <div className="flex flex-col items-start w-[20%] gap-y-2">
          <button className="font-bold">
            <Trans id="Quick Link" />
          </button>
          <button>
            <Trans id="Privacy Policy" />
          </button>
          <button>
            <Trans id="Terms Of Use" />
          </button>
          <button>FAQ</button>
          <button>
            <Trans id="Contact" />
          </button>
        </div>

        <div className="flex flex-col items-start w-[200px]">
          <span className="font-bold pb-4">
            <Trans id="Download App" />
          </span>
          <span className="text-gray leading-6 lgScreen:text-[12px] mdScreen:text-[10px] smScreen:text-[8px] text-[6px]">
            <Trans id="Save $3 with App New User Only" />
          </span>
          <div className="flex pt-2 gap-x-1">
            <img src="/img/QRBank.png" className="w-[100px]" />
            <div className="flex flex-col justify-between w-[100px] h-[100px]">
              <img src="/img/GooglePlay.svg" className="w-full h-[100px]" />
              <img src="/img/AppStore.svg" className="w-full h-[100px]" />
            </div>
          </div>
        </div>
      </div>

      <div className="h-[12%] spacingContent text-gray flex justify-center items-center gap-x-1">
        <img src="/img/iconCopyright.svg" />
        <span>Copyright Rimel 2022. All right reserved</span>
      </div>
    </div>
  );
};

export default Footer;
