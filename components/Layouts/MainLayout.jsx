import React, { useEffect, useState } from "react";
import { Trans } from "@lingui/macro";
import { getScreenHeight } from "../../utils/getScreenHeight";
import Footer from "./Footer.jsx";
import MenuTop from "./MenuTop.jsx";

const MainLayout = ({ children }) => {
  const [screenHeight, setScreenHeight] = useState(getScreenHeight());
  const bodyHeight = screenHeight - 440;

  useEffect(() => {
    const handleResize = () => {
      setScreenHeight(getScreenHeight());
    };
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div className="min-h-screen w-full relative">
      <MenuTop />
      <div className="h-[130px]" />
      <div style={{ minHeight: bodyHeight, backgroundColor: "#eeeee4" }}>
        {children}
      </div>
      <Footer />
    </div>
  );
};

export default MainLayout;
