import { Trans, t } from "@lingui/macro";
import React, { useEffect, useRef, useState } from "react";
import LangSwitcher from "../ChangeLanguageButton/LangSwitcher.jsx";
import { useRouter } from "next/router";
import { useRecoilState } from "recoil";
import { userLogin } from "../../atoms/userLogin.js";
import { getCurrentUser } from "../../utils/request.js";
import { Button } from "antd";
import { valueSearchProduct } from "../../atoms/searchProduct.js";

const MenuTop = () => {
  const router = useRouter();
  const [curUser, setCurUser] = useRecoilState(userLogin);
  const [searchValueAtom, setSearchValueAtom] =
    useRecoilState(valueSearchProduct);

  const [searchValue, setSearchValue] = useState("");
  const [scroll, setScroll] = useState(true);
  const [isHoverAvatar, setIsHoverAvatar] = useState(false);
  const modalAvaRef = useRef();

  const handleLogout = () => {
    setCurUser(null);
    setIsHoverAvatar(false);
    localStorage?.removeItem("curUser");
    localStorage?.removeItem("token");
    router.push("/login");
  };

  //Xử lý scroll
  const handleScroll = () => {
    const scrollTop =
      document.documentElement.scrollTop || document.body.scrollTop;
    if (scrollTop >= 50) {
      // Cuộn xuống 50px
      setScroll(false);
    }
    if (scrollTop === 0) {
      // Cuộn lên trên cùng
      setScroll(true);
    }
  };
  useEffect(() => {
    // Gán hàm xử lý sự kiện cuộn cho cửa sổ trình duyệt
    window.addEventListener("scroll", handleScroll);

    // Cleanup: Hủy bỏ hàm xử lý sự kiện khi component bị hủy
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const handleClickOutside = (e) => {
    if (modalAvaRef.current && !modalAvaRef.current.contains(e.target)) {
      setIsHoverAvatar(!isHoverAvatar);
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => document.removeEventListener("mousedown", handleClickOutside);
  });

  const search = (e) => {
    if (e.key === "Enter") {
      setSearchValueAtom(searchValue);
    }
  };

  return (
    <div
      className={`${
        scroll ? "h-[130px]" : "h-[55px]"
      } border-b-[1px] border-black05 w-full fixed bg-menuBg z-[101]`}
    >
      {scroll && (
        <div className="relative gap-x-4 flex mdScreen:justify-center spacingContent items-center bg-black text-white w-full h-[40px]">
          <Trans id="Summer Sale For All Swim Suits And Free Express Delivery - OFF 50%!" />

          <div
            onClick={() => {
              router.push("/");
            }}
            className="hover:animate-none animate-tada underline font-semibold hover:cursor-pointer hover:scale-110"
          >
            <Trans id="ShopNow" />
          </div>

          <LangSwitcher className="absolute lgScreen:right-[14%] mdScreen:right-[12%] smScreen:right-[10%] right-[7%] flex items-center lgScreen:h-[35px] mdScreen:h-[30px] h-[25px]" />
        </div>
      )}

      <div
        className={`spacingContent flex justify-between items-end ${
          scroll ? "h-[75px]" : "h-[50px]"
        }`}
      >
        <div className="w-[10%] flex items-end animate-slide-left xlScreen:text-5xl lgScreen:text-4xl mdScreen:text-2xl text-xl font-bold hover: cursor-pointer">
          <Trans id="Exclusive" />
        </div>

        <div className="w-[20%] flex items-end xlScreen:gap-x-14 lgScreen:gap-x-12 mdScreen:gap-x-10 gap-x-8 animate-slide-down">
          <div
            className={`hover:scale-110 hover:cursor-pointer hover:font-bold ${
              router.pathname === "/"
                ? "underline underline-offset-4 decoration-black05 font-bold scale-110"
                : ""
            }`}
            onClick={() => router.push("/")}
          >
            <Trans id="Home" />
          </div>
          <div
            className={`hover:scale-110 hover:cursor-pointer hover:font-bold ${
              router.pathname === "/contact"
                ? "underline underline-offset-4 decoration-black05 font-bold scale-110"
                : ""
            }`}
            onClick={() => router.push("/contact")}
          >
            <Trans id="Contact" />
          </div>
          <div
            className={`hover:scale-110 hover:cursor-pointer hover:font-bold ${
              router.pathname === "/about"
                ? "underline underline-offset-4 decoration-black05 font-bold scale-110"
                : ""
            }`}
            onClick={() => router.push("/about")}
          >
            <Trans id="About" />
          </div>
        </div>

        <div className="w-[40%] relative flex items-center justify-end lgScreen:gap-x-4 gap-x-2 animate-slide-right">
          <div className="lgScreen:h-[35px] mdScreen:h-[30px] h-[25px] w-[60%] relative">
            <input
              className="bg-[#F5F5F5] rounded-md flex justify-end placeholder:opacity-50 h-full w-full pl-4 pr-12"
              placeholder={t`What are you looking for?`}
              value={searchValue}
              onChange={(e) => {
                setSearchValue(e.target.value);
              }}
              onKeyDown={(e) => {
                search(e);
              }}
            />

            <div className="absolute right-2 -top-1 lgScreen:h-[25px] mdScreen:h-[20px] h-[15px]">
              <div className="flex justify-center items-center lgScreen:h-[40px] mdScreen:h-[35px] h-[30px]">
                {searchValueAtom === "" ? (
                  <img
                    onClick={() => setSearchValueAtom(searchValue)}
                    src="/img/iconSearch.png"
                    className="lgScreen:h-[20px] mdScreen:h-[15px] h-[10px] hover:cursor-pointer"
                  />
                ) : (
                  <img
                    onClick={() => {
                      setSearchValue("");
                      setSearchValueAtom("");
                    }}
                    src="/img/icon-reset.png"
                    className="lgScreen:h-[25px] mdScreen:h-[20px] h-[15px] hover:cursor-pointer"
                  />
                )}
              </div>
            </div>
          </div>

          {!curUser && (
            <>
              <Button
                className="lgScreen:h-[35px] mdScreen:h-[30px] h-[25px] flex justify-center items-center"
                type="primary"
                danger
                onClick={() => router.push("/login")}
              >
                <Trans id="Login" />
              </Button>
              <Button
                className="lgScreen:h-[35px] mdScreen:h-[30px] h-[25px] flex justify-center items-center"
                type="primary"
                danger
                onClick={() => router.push("/sign-up")}
              >
                <Trans id="Sign up" />
              </Button>
            </>
          )}

          {/* Khi đã login */}
          {curUser && (
            <>
              <img
                src="/img/iconHeart.svg"
                className="lgScreen:h-[25px] mdScreen:h-[20px] h-[15px] hover:scale-110 hover:cursor-pointer"
              />
              <img
                src="/img/iconCart.svg"
                className="lgScreen:h-[25px] mdScreen:h-[20px] h-[15px] hover:scale-110 hover:cursor-pointer"
              />
              <img
                onClick={() => {
                  setIsHoverAvatar(!isHoverAvatar);
                }}
                src="http://cdn.onlinewebfonts.com/svg/img_258083.png"
                className="rounded-full lgScreen:h-[25px] mdScreen:h-[20px] h-[15px] lgScreen:w-[25px] mdScreen:w-[20px] w-[15px] hover:scale-110 hover:cursor-pointer"
              />
              {isHoverAvatar && (
                <div className="text-white px-4 py-2 w-fit flex flex-col gap-y-3 rounded-md bg-gray absolute lgScreen:top-[30px] mdScreen:top-[25px] top-[20px] right-0">
                  <span className="flex items-center gap-x-2 hover:cursor-pointer hover:scale-105">
                    <img
                      className="w-[20px]"
                      src="/img/iconAccountHoverAva.svg"
                      alt=""
                    />
                    <Trans id="Manage My Account" />
                  </span>
                  <span className="flex items-center gap-x-2 hover:cursor-pointer hover:scale-105">
                    <img
                      className="w-[20px]"
                      src="/img/iconOderHoverAva.svg"
                      alt=""
                    />
                    <Trans id="My Order" />
                  </span>
                  <span className="flex items-center gap-x-2 hover:cursor-pointer hover:scale-105">
                    <img
                      className="w-[20px]"
                      src="/img/iconCancelHoverAva.svg"
                      alt=""
                    />
                    <Trans id="My Cancellations" />
                  </span>
                  <span className="flex items-center gap-x-2 hover:cursor-pointer hover:scale-105">
                    <img
                      className="w-[20px]"
                      src="/img/iconReviewHoverAva.svg"
                      alt=""
                    />
                    <Trans id="My Reviews" />
                  </span>
                  <span
                    onClick={handleLogout}
                    className="flex items-center gap-x-2 hover:cursor-pointer hover:scale-105"
                  >
                    <img
                      className="w-[20px]"
                      src="/img/iconLogoutHoverAva.svg"
                      alt=""
                    />
                    <Trans id="Logout" />
                  </span>
                </div>
              )}
            </>
          )}

          {!scroll && (
            <LangSwitcher className="flex items-center lgScreen:h-[35px] mdScreen:h-[30px] h-[25px]" />
          )}
        </div>
      </div>
    </div>
  );
};

export default MenuTop;
