import React, { useState, useEffect } from "react";

const ScrollToTop = () => {
  const [isScrolled, setIsScrolled] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 200) {
        setIsScrolled(true);
      } else {
        setIsScrolled(false);
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const scrollToTop = () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  return isScrolled ? (
    <button
      className="fixed font-bold right-10 bottom-6 border-2 border-gray rounded-full"
      onClick={scrollToTop}
    >
      <img src="/img/scroll-to-top.png" className="w-8 h-8" />
    </button>
  ) : null;
};

export default ScrollToTop;
