import React from "react";

const TitleHome = ({ LTitle, STitle, noOfProducts = null, className }) => {
  return (
    <div className={`flex flex-col gap-y-3 mb-2 ${className}`}>
      <div className="flex items-center gap-x-2">
        <div className="w-[20px] h-[35px] bg-red rounded-[3px]" />
        <div className="text-red font-bold">{LTitle}</div>
      </div>
      <div className="xlScreen:text-5xl lgScreen:text-4xl mdScreen:text-3xl text-2xl">
        {STitle}
        {noOfProducts ? ` (${noOfProducts})` : ""}
      </div>
    </div>
  );
};

export default TitleHome;
