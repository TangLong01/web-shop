export const TYPE_LIST = {
  0: "all",
  1: "clothe",
  2: "shoe",
  3: "socks",
  4: "watch",
  5: "glasses",
  6: "hat",
};
