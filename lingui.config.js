module.exports = {
  locales: ["en", "vi"],
  sourceLocale: "en",
  catalogs: [
    {
      path: "<rootDir>/locale/{locale}",
      include: ["<rootDir>/"],
      exclude: ["**/node_modules/**"],
    },
  ],
};
