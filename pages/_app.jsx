import "../styles/global.css";
import { i18n } from "@lingui/core";
import { I18nProvider } from "@lingui/react";
import en from "../locale/en";
import vi from "../locale/vi";
import { useEffect } from "react";
import MainLayout from "../components/Layouts/MainLayout";
import { ChangeLanguage } from "../utils/language";
import Head from "next/head";
import { ToastContainer } from "react-toastify";
import { injectStyle } from "react-toastify/dist/inject-style";
import { RecoilRoot, useRecoilState } from "recoil";
import { userLogin } from "../atoms/userLogin";
import ScrollToTop from "../components/ScrollToTop/ScrollToTop";

i18n.loadLocaleData("en", { plurals: en });
i18n.loadLocaleData("vi", { plurals: vi });

const MyAppWrapper = ({ children }) => {
  const [curUser, setCurUser] = useRecoilState(userLogin);

  useEffect(() => {
    injectStyle();

    if (localStorage?.getItem("curUser")) {
      setCurUser(localStorage?.getItem("curUser"));
    } else {
      localStorage?.removeItem("curUser");
      setCurUser(null);
    }

    if (localStorage?.getItem("curLang")) {
      ChangeLanguage(localStorage?.getItem("curLang"));
    } else {
      ChangeLanguage("en");
    }
  }, []);

  return children;
};

export default function Page({ Component, pageProps }) {
  return (
    <RecoilRoot>
      <MyAppWrapper>
        <I18nProvider i18n={i18n} forceRenderOnLocaleChange={true}>
          <Head>
            <title>Shop VIPRO</title>
            <link rel="icon" href="/img/IconWebShop.svg" />
          </Head>

          <ToastContainer autoClose={1500} />
          <MainLayout>
            <Component {...pageProps} />
          </MainLayout>
        </I18nProvider>
        <ScrollToTop />
      </MyAppWrapper>
    </RecoilRoot>
  );
}
