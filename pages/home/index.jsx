import React, { useEffect, useState, useRef, use } from "react";
import { useRecoilState, useRecoilValue } from "recoil";
import { userLogin } from "../../atoms/userLogin";
import { getAllProducts } from "../../utils/request";
import FlashSales from "../../components/Home/FlashSales";
import Categories from "../../components/Home/Categories";
import BestSelling from "../../components/Home/BestSelling";
import { TYPE_LIST } from "../../constants/type";
import Poster from "../../components/Home/Poster";
import OurProducts from "../../components/Home/OurProducts";
import NewArrival from "../../components/Home/NewArrival";
import Treatment from "../../components/Home/Treatment";
import { valueSearchProduct } from "../../atoms/searchProduct";

const Home = () => {
  const [curUser, setCurUser] = useRecoilState(userLogin);
  const searchValue = useRecoilValue(valueSearchProduct);

  const [data, setData] = useState([]);
  const [dataOurProducts, setDataOurProducts] = useState([]);
  const [chooseType, setChooseType] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(8);

  const categoriesRef = useRef(null);

  useEffect(() => {
    try {
      if (chooseType === 0) {
        getAllProducts().then((res) => {
          setData(res.data);
        });
      } else {
        getAllProducts({ typeEn: TYPE_LIST[chooseType] }).then((res) => {
          setData(res.data);
        });
      }
    } catch (e) {
      console.log(e);
    }
  }, [chooseType]);

  useEffect(() => {
    try {
      if (chooseType === 0) {
        getAllProducts({
          _page: currentPage,
          _limit: pageSize,
          _sort: "id",
          _order: "desc",
        }).then((res) => {
          setDataOurProducts(res.data);
        });
      } else {
        getAllProducts({
          typeEn: TYPE_LIST[chooseType],
          _page: 1,
          _limit: 3,
          _sort: "id",
          _order: "desc",
        }).then((res) => {
          setDataOurProducts(res.data);
        });
      }
    } catch (e) {
      console.log(e);
    }
  }, [chooseType, currentPage, pageSize]);

  return (
    <div className="spacingContent relative">
      <Poster />
      {searchValue === "" && (
        <div>
          <Categories
            chooseType={chooseType}
            setChooseType={setChooseType}
            categoriesRef={categoriesRef}
          />
          <FlashSales data={data} />
          <BestSelling data={data} />
        </div>
      )}
      <OurProducts
        dataOurProducts={dataOurProducts}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        pageSize={pageSize}
        setPageSize={setPageSize}
        chooseType={chooseType}
      />
      <NewArrival />
      <Treatment />
    </div>
  );
};

export default Home;
