import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import { Trans, t } from "@lingui/macro";
import { Button, Input } from "antd";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { toast } from "react-toastify";
import { useRecoilState } from "recoil";
import { userLogin } from "../../atoms/userLogin";
import { loginAccount } from "../../utils/auth";

const LoginPage = () => {
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [curUser, setCurUser] = useRecoilState(userLogin);

  const handleLogin = async () => {
    try {
      if (email !== "" && password !== "") {
        await loginAccount({
          email: email,
          password: password,
        }).then((res) => {
          if (res.data) {
            toast.success(t`Successful account registration`);
            router.push("/");
            setCurUser(res.data.result);
            console.log(res.data);
            localStorage.setItem("token", res.data.token);
          }
        });
      } else toast.error(t`Please enter your email and password`);
    } catch {
      toast.error(t`Wrong email or password`);
    }
  };

  return (
    <div className="pt-[3%] pb-[6%] flex spacingContent">
      <img src="/img/imgSignIn.svg" className="w-[55%]" />
      <div className="w-[45%] gap-y-4 flex flex-col justify-center h-full pl-[17%] mt-[10%]">
        <span className="text-4xl font-bold">
          <Trans id="Log in to Exclusive" />
        </span>
        <span>
          <Trans id="Enter your details below" />
        </span>
        <span className="flex flex-col gap-y-1 mt-2">
          <Trans id="Email" />
          <Input
            placeholder={t`Enter your email`}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            onPressEnter={handleLogin}
          />
        </span>
        <span className="flex flex-col gap-y-1">
          <Trans id="Password" />
          <Input.Password
            placeholder={t`Enter your password`}
            iconRender={(visible) =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            onPressEnter={handleLogin}
          />
        </span>
        <span className="mt-2 w-full flex">
          <Button onClick={handleLogin} type="primary" danger className="w-1/2">
            <Trans id="Login" />
          </Button>
          <span
            onClick={() => router.push("login/recovery-password")}
            className="w-1/2 text-red flex justify-end items-center hover:cursor-pointer hover:underline underline-offset-4"
          >
            <Trans id="Forget Password?" />
          </span>
        </span>
      </div>
    </div>
  );
};

export default LoginPage;
