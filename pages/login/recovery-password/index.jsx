import { Trans, t } from "@lingui/macro";
import { Button, Input, Modal, Spin } from "antd";
import React, { useState } from "react";
import { toast } from "react-toastify";
import { recoveryPasswordAccount } from "../../../utils/auth";
import { LoadingOutlined } from "@ant-design/icons";

const RecoveryPasswordPage = () => {
  const domain = `${window.location.protocol}//${window.location.host}`;
  const [email, setEmail] = useState("");

  const [showModalNotify, setShowModalNotify] = useState(false);
  const [sent, setSent] = useState(false);

  const handleForgetPassword = async () => {
    setShowModalNotify(false);
    setSent(false);
    try {
      if (email !== "") {
        setShowModalNotify(true);
        await recoveryPasswordAccount({
          email: email,
          title: "Recovery your password Shop VIPRO",
          link: `${domain}/login/reset-password`,
        }).then((res) => {
          if (res.data) {
            setSent(true);
            localStorage.setItem(
              "tokenToRecoveryPassword",
              res.data.or_use_this_token
            );
            localStorage.setItem("emailToRecoveryPassword", email);
          }
        });
      } else toast.error(t`Please enter your email account`);
    } catch {
      toast.error(t`Email does not exist in the system`);
    }
  };

  return (
    <div className="pt-[3%] pb-[6%] flex spacingContent">
      <img src="/img/imgSignIn.svg" className="w-[55%]" />
      <div className="w-1/2 gap-y-4 flex flex-col justify-center h-full pl-[17%] mt-[10%]">
        <span className="text-4xl font-bold">
          <Trans id="Recovery your password" />
        </span>
        <span>
          <Trans id="Enter your details below" />
        </span>
        <span className="flex flex-col gap-y-1 mt-2">
          <Trans id="Your email account" />
          <Input
            placeholder={t`Enter your email account`}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            onPressEnter={handleForgetPassword}
          />
        </span>
        <span onClick={handleForgetPassword} className="mt-2 w-full">
          <Button type="primary" danger className="w-full">
            <Trans id="Send to your email" />
          </Button>
        </span>
      </div>

      <Modal
        title={
          sent ? (
            t`Sent!`
          ) : (
            <span className="flex items-center">
              <Trans id="Sending" />
              <Spin
                indicator={
                  <LoadingOutlined
                    style={{ fontSize: 20, marginLeft: 10 }}
                    spin
                  />
                }
              />
            </span>
          )
        }
        centered
        open={showModalNotify}
        footer={[
          <Button
            key="ok"
            type="primary"
            onClick={() => {
              setShowModalNotify(false);
            }}
          >
            OK
          </Button>,
        ]}
      >
        {sent ? (
          <>
            <Trans id="Please check your email!" />
            <br />
            <Trans id="If you do not receive it, please check the email you provided!" />
          </>
        ) : (
          <Trans id="Please wait!" />
        )}
      </Modal>
    </div>
  );
};

export default RecoveryPasswordPage;
