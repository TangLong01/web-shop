import { Trans, t } from "@lingui/macro";
import { Button, Input } from "antd";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { resetPassword } from "../../../utils/auth";
import InvalidInput from "../../../components/InvalidInput/InvalidInput";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";

const RecoveryPasswordPage = () => {
  const router = useRouter();
  const [newPassword, setNewPassword] = useState("");
  const [reNewPassword, setReNewPassword] = useState("");

  const [checkNewPassword, setCheckNewPassword] = useState(false);
  const [checkReNewPassword, setCheckReNewPassword] = useState(false);

  useEffect(() => {
    const regexPassword = /^(?=.*[A-Z])[A-Za-z\d]{6,}$/;
    setCheckNewPassword(regexPassword.test(newPassword));

    if (newPassword === reNewPassword) {
      setCheckReNewPassword(true);
    } else setCheckReNewPassword(false);
  }, [newPassword, reNewPassword]);

  const handleResetPassword = async () => {
    try {
      if (checkNewPassword && newPassword !== "" && reNewPassword !== "") {
        await resetPassword({
          email: localStorage.getItem("emailToRecoveryPassword"),
          password: newPassword,
          password_confirmation: reNewPassword,
          token: localStorage.getItem("tokenToRecoveryPassword"),
        }).then((res) => {
          if (res.data) {
            toast.success(t`Password reset successful! Please login!`);
            localStorage.removeItem("emailToRecoveryPassword");
            localStorage.removeItem("tokenToRecoveryPassword");
            router.push("/login");
          }
        });
      } else toast.error(t`Invalid password`);
    } catch {
      toast.error(t`Invalid password`);
    }
  };

  return (
    <div className="pt-[3%] pb-[6%] flex spacingContent">
      <img src="/img/imgSignIn.svg" className="w-[55%]" />
      <div className="w-1/2 gap-y-4 flex flex-col justify-center h-full pl-[17%] mt-[10%]">
        <span className="text-4xl font-bold">
          <Trans id="Reset your password" />
        </span>
        <span>
          <Trans id="Enter your details below" />
        </span>
        <span className="flex flex-col gap-y-1">
          <Trans id="New password" />
          <Input.Password
            placeholder={t`Enter your new password`}
            iconRender={(visible) =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
            value={newPassword}
            onChange={(e) => setNewPassword(e.target.value)}
            onPressEnter={handleResetPassword}
            status={!checkNewPassword && newPassword !== "" && "error"}
          />
          {!checkNewPassword && newPassword !== "" && (
            <InvalidInput
              invalid="Password invalidate"
              invalitChild={[
                "At least 6 characters",
                "At least 1 uppercase character",
                "Does not contain special characters",
              ]}
            />
          )}
        </span>
        <span className="flex flex-col gap-y-1">
          <Trans id="Re-Enter new password" />
          <Input.Password
            placeholder={t`Re-Enter your new password`}
            iconRender={(visible) =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
            value={reNewPassword}
            onChange={(e) => setReNewPassword(e.target.value)}
            onPressEnter={handleResetPassword}
            status={!checkReNewPassword && reNewPassword !== "" && "error"}
          />
          {!checkReNewPassword && reNewPassword !== "" && (
            <InvalidInput invalid="Re-Password needs to be the same as password" />
          )}
        </span>
        <span onClick={handleResetPassword} className="mt-2 w-full">
          <Button type="primary" danger className="w-full">
            <Trans id="Send to your email" />
          </Button>
        </span>
      </div>
    </div>
  );
};

export default RecoveryPasswordPage;
