import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import { Trans, t } from "@lingui/macro";
import { Button, Input } from "antd";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import InvalidInput from "../../components/InvalidInput/InvalidInput";
import { registerAccount } from "../../utils/auth";

const SignUpPage = () => {
  const router = useRouter();
  const [name, setName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [rePassword, setRePassword] = useState("");

  const [checkEmail, setCheckEmail] = useState(false);
  const [checkPhoneNumber, setCheckPhoneNumber] = useState(false);
  const [checkPassword, setCheckPassword] = useState(false);
  const [checkRePassword, setCheckRePassword] = useState(false);

  useEffect(() => {
    const regexPhoneNumber = /^\d+$/;
    setCheckPhoneNumber(regexPhoneNumber.test(phoneNumber));

    const regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    setCheckEmail(regexEmail.test(email));

    const regexPassword = /^(?=.*[A-Z])[A-Za-z\d]{6,}$/;
    setCheckPassword(regexPassword.test(password));

    if (password === rePassword) {
      setCheckRePassword(true);
    } else setCheckRePassword(false);
  }, [phoneNumber, email, password, rePassword]);

  const handleCreateAccount = async () => {
    const error =
      checkEmail && checkPhoneNumber && checkPassword && checkRePassword;
    try {
      if (
        error &&
        name !== "" &&
        phoneNumber !== "" &&
        email !== "" &&
        password !== "" &&
        rePassword !== ""
      ) {
        await registerAccount({
          name: name,
          phone: phoneNumber,
          email: email,
          password: password,
          password_confirmation: rePassword,
        }).then((res) => {
          if (res.data) {
            toast.success(t`Successful account registration! Please login!`);
            router.push("/login");
          }
        });
      } else toast.warning(t`Invalid account information`);
    } catch (error) {
      error.response.data.errors.map((item, index) => {
        if (item === "The email has already been taken.") {
          toast.warning(t`The email has already been taken`);
        } else if (item === "The phone must be 10 digits.") {
          toast.warning(t`The phone must be 10 digits`);
        }
      });
    }
  };

  return (
    <div className="pt-[3%] pb-[6%] flex spacingContent">
      <img src="/img/imgSignIn.svg" className="w-[55%]" />
      <div className="w-1/2 gap-y-4 flex flex-col justify-center h-full pl-[17%] mt-[3%]">
        <span className="text-4xl font-bold">
          <Trans id="Create an account" />
        </span>
        <span>
          <Trans id="Enter your details below" />
        </span>
        <span className="mt-2">
          <Trans id="Name" />
          <Input
            placeholder={t`Enter your name`}
            value={name}
            onChange={(e) => setName(e.target.value)}
            onPressEnter={handleCreateAccount}
          />
        </span>
        <span className="flex flex-col gap-y-1">
          <Trans id="Phone number" />
          <Input
            placeholder={t`Enter your phone number`}
            value={phoneNumber}
            onChange={(e) => setPhoneNumber(e.target.value)}
            onPressEnter={handleCreateAccount}
            status={!checkPhoneNumber && phoneNumber !== "" && "error"}
          />
          {!checkPhoneNumber && phoneNumber !== "" && (
            <InvalidInput invalid="Phone number invalidate" />
          )}
        </span>
        <span className="flex flex-col gap-y-1">
          <Trans id="Email" />
          <Input
            placeholder={t`Enter your email`}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            onPressEnter={handleCreateAccount}
            status={!checkEmail && email !== "" && "error"}
          />
          {!checkEmail && email !== "" && (
            <InvalidInput invalid="Email invalidate" />
          )}
        </span>
        <span className="flex flex-col gap-y-1">
          <Trans id="Password" />
          <Input.Password
            placeholder={t`Enter your password`}
            iconRender={(visible) =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            onPressEnter={handleCreateAccount}
            status={!checkPassword && password !== "" && "error"}
          />
          {!checkPassword && password !== "" && (
            <InvalidInput
              invalid="Password invalidate"
              invalitChild={[
                "At least 6 characters",
                "At least 1 uppercase character",
                "Does not contain special characters",
              ]}
            />
          )}
        </span>
        <span className="flex flex-col gap-y-1">
          <Trans id="Re-Password" />
          <Input.Password
            placeholder={t`Re-Enter your password`}
            iconRender={(visible) =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
            value={rePassword}
            onChange={(e) => setRePassword(e.target.value)}
            onPressEnter={handleCreateAccount}
            status={!checkRePassword && rePassword !== "" && "error"}
          />
          {!checkRePassword && rePassword !== "" && (
            <InvalidInput invalid="Re-Password needs to be the same as password" />
          )}
        </span>
        <span onClick={handleCreateAccount} className="mt-2 w-full">
          <Button type="primary" danger className="w-full">
            <Trans id="Create account" />
          </Button>
        </span>
      </div>
    </div>
  );
};

export default SignUpPage;
