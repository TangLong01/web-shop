const jsonServer = require("json-server");
const server = jsonServer.create();
const router = jsonServer.router("db.json");
const middlewares = jsonServer.defaults();

server.use(jsonServer.bodyParser);
server.use(middlewares);

// Middleware tùy chỉnh để cập nhật tổng số sản phẩm
server.use((req, res, next) => {
  if (req.method === "POST" && req.url === "/products") {
    const db = router.db;
    const totalProducts = db.get("products").size().value();
    db.set("summary.totalProducts", totalProducts).write();
  }
  next();
});

server.use(router);
server.listen(8000, () => {
  console.log("JSON Server is running");
});
