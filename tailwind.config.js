/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: "jit",
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        black05: "rgba(0, 0, 0, 0.5)",
        gray: "#3D3D3D",
        red: "#FF4D4F",
        primary: "#0A59DA",
        mainBg: "#eeeee4",
        menuBg: "#E6E6FA",
      },
      screens: {
        smScreen: "800px",
        mdScreen: "1224px",
        lgScreen: "1600px",
        xlScreen: "1920px",
      },

      keyframes: {
        wave: {
          "0%": { transform: "rotate(0.0deg)" },
          "10%": { transform: "rotate(14deg)" },
          "20%": { transform: "rotate(-8deg)" },
          "30%": { transform: "rotate(14deg)" },
          "40%": { transform: "rotate(-4deg)" },
          "50%": { transform: "rotate(10.0deg)" },
          "60%": { transform: "rotate(0.0deg)" },
          "100%": { transform: "rotate(0.0deg)" },
        },
        slideDownIn: {
          "0%": {
            "-webkit-transform": "translateY(-100%)",
            transform: "translateY(-100%)",
            visibility: "visible",
          },
          "100%": {
            transform: "translateY(0)",
            "-webkit-transform": "translateY(0)",
          },
        },
        slideUpIn: {
          "0%": {
            "-webkit-transform": "translateY(100%)",
            transform: "translateY(100%)",
            visibility: "visible",
          },
          "100%": {
            transform: "translateY(0)",
            "-webkit-transform": "translateY(0)",
          },
        },
        slideLeftIn: {
          "0%": {
            "-webkit-transform": "translateX(-100%)",
            transform: "translateX(-100%)",
            visibility: "visible",
          },
          "100%": {
            transform: "translateX(0)",
            "-webkit-transform": "translateX(0)",
          },
        },
        slideRightIn: {
          "0%": {
            "-webkit-transform": "translateX(100%)",
            transform: "translateX(100%)",
            visibility: "visible",
          },
          "100%": {
            transform: "translateX(0)",
            "-webkit-transform": "translateX(0)",
          },
        },
        tada: {
          "0%": {
            "-webkit-transform": "translateX(100%)",
            transform: "translateX(100%)",
            visibility: "visible",
          },
          "100%": {
            transform: "translateX(0)",
            "-webkit-transform": "translateX(0)",
          },
        },
        vipBounce: {
          "0%": { transform: "translateY(0%)" },
          "50%": { transform: "translateY(20px)" },
          "100%": { transform: "translateY(0%)" },
        },
        tada: {
          "0%": { transform: "scale(1)" },
          "10%, 20%": { transform: "scale(0.9) rotate(-2deg)" },
          "30%, 50%, 70%, 90%": { transform: "scale(1.1) rotate(2deg)" },
          "40%, 60%, 80%": { transform: "scale(1.1) rotate(-2deg)" },
          "100%": { transform: "scale(1)" },
        },
        none: {},
      },
      animation: {
        none: "",
        tada: "tada 1s infinite",
        waving: "wave 1s linear infinite",
        "slide-up": "slideUpIn 1s linear",
        "slide-down": "slideDownIn 1s linear",
        "slide-left": "slideLeftIn 1s linear",
        "slide-right": "slideRightIn 1s linear",
        vipbounce: "vipBounce 2s linear infinite",
      },
    },
  },
  plugins: [require("tailwindcss-line-clamp")],
};
