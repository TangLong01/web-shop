import axios from "axios";

const baseUrlApi = "http://localhost:8000";
export const request = (method, url, params = null, data = null) => {
  return axios({
    method,
    url: `${baseUrlApi}${url}`,
    params,
    data,
  });
};
