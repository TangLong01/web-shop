import axios from "axios";

export const registerAccount = (data) => {
  return axios.post("https://apingweb.com/api/register", data);
};

export const loginAccount = (data) => {
  return axios.post("https://apingweb.com/api/login", data);
};

export const recoveryPasswordAccount = (data) => {
  return axios.post("https://apingweb.com/api/forgot-password", data);
};

export const resetPassword = (data) => {
  return axios.post("https://apingweb.com/api/reset-password", data);
};

export const getAllUsers = (data) => {
  return axios.get("https://apingweb.com/api/users", data);
};
