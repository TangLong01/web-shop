import { i18n } from "@lingui/core";

export const ChangeLanguage = async (lang) => {
  const { messages } = await import(`../locale/${lang}.js`);
  i18n.load(lang, messages);
  i18n.activate(lang);
  if (typeof window !== "undefined") {
    localStorage?.setItem("curLang", lang);
  }
};

export const GetCurrentLanguage = () => {
  if (typeof window !== "undefined") {
    var curLang = localStorage?.getItem("curLang");
    curLang = curLang == null ? "en" : curLang;
    return curLang;
  } else {
    return "en";
  }
};
