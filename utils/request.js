import { request } from "./api";

export const getAllProducts = (params) => request("get", `/products`, params);

export const getTotalProducts = () => request("get", `/summary`);
